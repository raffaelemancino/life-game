var app = new Vue({
    el: '#app',
    data: function() {
        return {
            n: 22,
            cells: [],
            interval: null,
        }
    },
    methods: {
        init: function()
        {
            this.cells = new Array(this.n);
            for (var i = 0; i < this.cells.length; i++)
            {
                this.cells[i] = new Array(this.n);
                for (var j = 0; j < this.cells[i].length; j++) {
                    //this.cells[i][j] = Math.round(Math.random());
                    this.cells[i][j] = 0;
                }
            }

            this.start_interval();
        },

        update: function()
        {
            var new_cells = JSON.parse(JSON.stringify(this.cells));

            for (var i = 0; i < new_cells.length; i++)
            {
                for (var j = 0; j < new_cells[i].length; j++)
                {
                    var test = this.getNearValue(i,j);
                    /*
                    ** Qualsiasi cella viva con meno di due celle vive adiacenti muore, come per effetto d'isolamento;
                    ** Qualsiasi cella viva con due o tre celle vive adiacenti sopravvive alla generazione successiva;
                    ** Qualsiasi cella viva con più di tre celle vive adiacenti muore, come per effetto di sovrappopolazione;
                    ** Qualsiasi cella morta con esattamente tre celle vive adiacenti diventa una cella viva, come per effetto di riproduzione
                    */
                    if (test<2 && new_cells[i][j]==1)
                    {
                        new_cells[i][j] = 0;
                    } else if (test>=2 && test<=3 && new_cells[i][j]==1) {
                        // nnt
                    } else if (test>3 && new_cells[i][j]==1) {
                        new_cells[i][j] = 0;
                    } else if (test==3 && new_cells[i][j]==0) {
                        new_cells[i][j] = 1;
                    }
                }
            }

            this.cells = new_cells;
        },

        getNearValue: function(i,j)
        {
            var total = 0;
            if (i>0)
                total += this.cells[i-1][j];
            if (j>0)
                total += this.cells[i][j-1];
            if (i<(this.n-1))
                total += this.cells[i+1][j];
            if (j<(this.n-1))
                total += this.cells[i][j+1];

            if (i>0 && j>0)
                total += this.cells[i-1][j-1];
            if (j>0 && i<(this.n-1))
                total += this.cells[i+1][j-1];
            if (i<(this.n-1) && j<(this.n-1))
                total += this.cells[i+1][j+1];
            if (i>0 && j<(this.n-1))
                total += this.cells[i-1][j+1];

            return total;
        },

        change: function(i,j)
        {
            if (this.cells[i][j] == 1)
            {
                this.cells[i][j] = 0;
            } else {
                this.cells[i][j] = 1;
            }
            this.$forceUpdate();
        },

        stop_interval: function()
        {
            clearInterval(this.interval);
        },

        start_interval: function()
        {
            this.interval = setInterval(this.update, 1000);
        },
    },

    beforeMount()
    {
        this.init();
    }
})
